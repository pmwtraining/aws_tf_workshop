# Uncomment the resources below and add the required arguments.

resource "aws_security_group" "x_x_x" {
  # 1. Define logical names (identifiers) for resource.
  #    Eg: resource "type" "resource_logical_name" {}
  #    Docs: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group

  # 2. Set the name of your security group below in format "studentx-sg"
  name = "Studentxx-sg"

  description = "Studentxx security group."
  vpc_id = "vpc-111111"
}

# To reference attributes of resources use syntax TYPE.NAME.ATTRIBUTE
#   for example, in order to create rule in specific secrurity group you will have to
#   refer security group by its name :
#     security_group_id = aws_security_group.mysecuritygroup.id
#
# Reference: https://www.terraform.io/docs/configuration/expressions/index.html

/*
resource "aws_security_group_rule" "ssh_ingress_access" {
  # 1. Add required arguments to open ingress(incoming) traffic to TCP port 22 - we'll use it later to ssh into the instance.
  # 2. Add argument to reference Security Group resource.
  # Docs:https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule
  
  # ...
  type = "xxxxxx"
  from_port = xx
  to_port = xx
  protocol = "xxx"
  security_group_id = aws_security_group.x.id
  cidr_blocks = [ "0.0.0.0/0" ] 
}

resource "aws_security_group_rule" "egress_access" {
  # 1. Add required arguments to open outgoing traffic to all ports (0-65535) 
  # 2. Add argument to reference Security Group resource.
  # Docs: https://www.terraform.io/docs/providers/aws/r/security_group_rule.html
  
  # ...
  type = "xxxxxx"
  from_port = xx
  to_port = xx
  protocol = "xxx"
  security_group_id = aws_security_group.x.id
  cidr_blocks = [ "0.0.0.0/0" ]
}

resource "aws_instance" "x_x" {
  # 1. Add resource name.
  # 2. Specify VPC subnet ID
  # 3. Specify EC2 instance type.
  # 4. Specify Security group for this instance (use one that we create above).
  # Docs: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance

  subnet_id = "subnet-11111111"

  instance_type = "x.x"
  vpc_security_group_ids = [ aws_security_group.x.id"]
  associate_public_ip_address = false
  # user_data = "${file("../shared/user-data.txt")}"
  tags {
    Name = "w1-studentxx-instance"
  }
  
  # Keep these arguments as is, if in N. Virginia: otherwise replace with the ami for Amazon Linux AMI 2018.03.0 (HVM)/availability_zone for your region
  # Check community ami's
  ami = "ami-00eb20669e0990cb4"
  availability_zone = "us-east-1c"

 # example for Ireland 	
 # ami = "ami-028188d9b49b32a80"
 # availability_zone = "eu-west-1c"

}
*/
