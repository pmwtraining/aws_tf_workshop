resource "aws_security_group" "studentxx_sg" {
  name = "studentx-sg"

  description = "studentxx security group."

  # This is fake VPC ID, please put a real one to make this configuration work
  vpc_id = "vpc-1111111"
}

resource "aws_security_group_rule" "ssh_ingress_access" {
  type = "ingress"
  from_port = 22
  to_port = 22
  protocol = "tcp"
  cidr_blocks = [ "0.0.0.0/0" ] 
  security_group_id = "${aws_security_group.w1_security_group.id}"
}

resource "aws_security_group_rule" "egress_access" {
  type = "egress"
  from_port = 0
  to_port = 65535
  protocol = "tcp"
  cidr_blocks = [ "0.0.0.0/0" ]
  security_group_id = "${aws_security_group.w1_security_group.id}"
}

resource "aws_instance" "studentxx_instance" {

# This is fake VPC subnet ID, please put a real one to make this config work
  subnet_id = "subnet-1111111"

  instance_type = "t2.nano"
  vpc_security_group_ids = [ "${aws_security_group.w1_security_group.id}" ]
  associate_public_ip_address = true
  user_data = "${file("../../shared/user-data.txt")}"
  tags {
    Name = "studentxx-myinstance"
  }
  
  ami = "ami-00eb20669e0990cb4"
  availability_zone = "us-east-1c"

  
}

